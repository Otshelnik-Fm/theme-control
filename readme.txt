== Установка/Обновление ==

<h3 style="text-align: center;">Установка:</h3>

1. В админке вашего сайта перейдите на страницу "WP-Recall" -> "Шаблоны" и в самом верху нажмите на кнопку "Обзор", выберите .zip архив дополнения на вашем пк и нажмите кнопку "Установить".
2. В списке загруженных шаблонов, на этой странице, найдите это дополнение, наведите на него курсор мыши и нажмите кнопку "Подключить".


<h3 style="text-align: center;">Обновление:</h3>
Дополнение поддерживает <a href="https://codeseller.ru/avtomaticheskie-obnovleniya-dopolnenij-plagina-wp-recall/" target="_blank">автоматическое обновление</a> - два раза в день отправляются вашим сервером запросы на обновление.
Если в течении суток вы не видите обновления (а на странице дополнения вы видите что версия вышла новая), советую ознакомиться с этой <a href="https://codeseller.ru/post-group/rabota-wordpress-krona-cron-prinuditelnoe-vypolnenie-kron-zadach-dlya-wp-recall/" target="_blank">статьёй</a>




== FAQ ==

= Нет отображения аватарки, да и внешний вид не тот, что на видео =
- По умолчанию ее нет. 
Но установив дополнение <a href="https://codeseller.ru/products/user-info-tab/" target="_blank">User Info Tab</a> - вы увидите все как на видео


= Какие дополнения нужно установить, чтобы было как на видео? =
- Это дополнение: <a href="https://codeseller.ru/products/user-info-tab/" target="_blank">User Info Tab</a> - и перейдите на страницу этого дополнения - во вкладке FAQ я отвечу на данный вопрос.


= Нажимаю на имя - страница перезагружается, что нужно сделать чтобы был ajax переход? =
- Этот функционал включен в дополнение <a href="https://codeseller.ru/products/user-info-tab/" target="_blank">User Info Tab</a>


= Есть демо? =
- Да: <a href="http://theme-control.otshelnik-fm.ru/" target="_blank">здесь</a>



== Changelog ==
= 2017-09-04 =
v1.1.1
* Небольшие изменения


= 2017-03-06 =
v1.1
* Работа с 16-й версией WP-Recall


= 2017-03-06 =
v1.0
* Release




== Поддержка и контакты ==

* Поддержка осуществляется в рамках текущего функционала дополнения
* При возникновении проблемы, создайте соотвествующую тему на <a href="https://codeseller.ru/forum/product-14506/" target="_blank">форуме поддержки</a> товара
* Если вам нужна доработка под ваши нужды - вы можете обратиться ко мне в <a href="https://codeseller.ru/author/otshelnik-fm/?tab=chat" target="_blank">ЛС</a> с техзаданием на платную доработку.

Полный список моих работ опубликован <a href="http://across-ocean.otshelnik-fm.ru/" target="_blank">на моем демо-сайте</a> и в каталоге магазина <a href="https://codeseller.ru/author/otshelnik-fm/?tab=publics&subtab=type-products" target="_blank">CodeSeller.ru</a>
